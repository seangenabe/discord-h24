{
  const f = new Intl.DateTimeFormat(
    'ja-JP', 
    { year: 'numeric', month: '2-digit', day: '2-digit', hour12: false, hour: '2-digit', minute: 'numeric' }
  )

  function process(t) {
    const time = new Date(Number(t.attributes.datetime.value))
    t.textContent = f.format(new Date(time))
  }

  function refresh() {
    const els = document.querySelectorAll('time[datetime]:not(.discord-h24-upgraded)')
    for (let t of els) {
      process(t)
    }
  }

  const observer = new MutationObserver(refresh)
  
  observer.observe(document.body, {
    childList: true,
    subtree: true
  })
}
